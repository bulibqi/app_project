function  loadCategoryLevel(pid,cl,categoryLevel){
	$.ajax({
		type:"GET",//请求类型
		url:"categorylevellist.json",//请求的url
		data:{pid:pid},//请求参数
		dataType:"json",//ajax接口（请求url）返回的数据类型
		success:function(data){//data：返回数据（json对象）
			
			$("#"+categoryLevel).html("");
			var options = "<option value=\"\">--请选择--</option>";
			for(var i = 0; i < data.length; i++){
				if(cl != null && cl != undefined && data[i].id == cl ){
					options += "<option selected=\"selected\" value=\""+data[i].id+"\" >"+data[i].categoryName+"</option>";
				}else{
					options += "<option value=\""+data[i].id+"\">"+data[i].categoryName+"</option>";
				}
			}
			$("#"+categoryLevel).html(options);
		},
		error:function(data){//当访问时候，404，500 等非200的错误状态码
			alert("加载分类列表失败！");
		}
	});
}   

function delfile(id){
	$.ajax({
		type:"GET",//请求类型
		url:"/AppManage/app/delfile",//请求的url
		data:{id:id},//请求参数
		dataType:"text",//ajax接口（请求url）返回的数据类型
		success:function(data){//data：返回数据（json对象）
			if(data == "true"){
				alert("删除成功！");
				$("#uploadfile").show();
				$("#logoFile").html('');
			}else if(data == "false"){
				alert("删除失败！");
			}
		},
		error:function(data){//当访问时候，404，500 等非200的错误状态码
			alert("请求错误！");
		}
	});  
}

$(function(){  
	//动态加载所属平台列表
	$.ajax({
		type:"GET",//请求类型
		url:"/AppManage/data/queryDicPt",//请求的url

		dataType:"json",//ajax接口（请求url）返回的数据类型
		success:function(data){//data：返回数据（json对象）
			var fid = $("#fid").val();
			$("#flatformId").html("");
			var options = "<option value=\"\">--请选择--</option>";
			for(var i = 0; i < data.length; i++){
				if(fid != null && fid != undefined && data[i].valueId == fid ){
					options += "<option selected=\"selected\" value=\""+data[i].valueId+"\" >"+data[i].valueName+"</option>";
				}else{
					options += "<option value=\""+data[i].valueId+"\">"+data[i].valueName+"</option>";
				}
			}
			$("#flatformId").html(options);
		},
		error:function(data){//当访问时候，404，500 等非200的错误状态码
			alert("加载平台列表失败！");
		}
	});
	
	$("#back").on("click",function(){
		window.location.href = "/AppManage/app/queryApp";
	});
	
	//LOGO图片---------------------
	var logoPicPath = $("#logoPicPath").val();
	var id = $("#id").val();
	if(logoPicPath == null || logoPicPath == "" ){
		$("#uploadfile").show();
	}else{
		$("#logoFile").append("<p><img src=\""+logoPicPath+"?m="+Math.random()+"\" width=\"100px;\"/> &nbsp;&nbsp;"+
							"<a href=\"javascript:;\" onclick=\"delfile('"+id+"');\">删除</a></p>");
		
	}
    $("#categoryLevel1").change(function(){
    	var categoryLevel2= $("#categoryLevel2");
        var categoryLevel3= $("#categoryLevel3");

    	$.ajax({
            type:"GET",//请求类型
            url:"/AppManage/cate/queryCate",
            data:{parentId:$(this).val()},
            dataType:"json",
            success:function(data){
                if(categoryLevel2 == null || categoryLevel2 == "" || categoryLevel2 == undefined) return;
                categoryLevel2.html("");
                var options = "<option value=\"-1\">--请选择--</option>";
                categoryLevel3.html("");
                var options = "<option value=\"-1\">--请选择--</option>";
                for(var i = 0; i < data.length; i++){
                    options += "<option value=\""+data[i].id+"\">"+data[i].categoryName+"</option>";

                }
                categoryLevel2.html(options);
            },
            error:function(data){
                alert("加载分类失败！");
            }
        });
    })
    $("#categoryLevel2").change(function(){
        var categoryLevel3= $("#categoryLevel3");
        $.ajax({
            type:"GET",//请求类型
            url:"/AppManage/cate/queryCate",
            data:{parentId:$(this).val()},
            dataType:"json",
            success:function(data){

                if(categoryLevel3 == null || categoryLevel3 == "" || categoryLevel3 == undefined) return;
                categoryLevel3.html("");
                var options = "<option value=\"-1\">--请选择--</option>";
                for(var i = 0; i < data.length; i++){
                    options += "<option value=\""+data[i].id+"\">"+data[i].categoryName+"</option>";

                }
                categoryLevel3.html(options);
            },
            error:function(data){
                alert("加载分类失败！");
            }
        });
    })


});
      
      
      