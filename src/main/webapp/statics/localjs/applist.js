$(".level").change(function(){
    var $nextLevel = $(this).parents("li").next().children().children(".col-md-6").children(".level");
    $.ajax({
        type:"GET",//请求类型
        url:"/AppManage/cate/queryCate",
        data:{parentId:$(this).val()},
        dataType:"json",
        success:function(data){
            if($nextLevel == null || $nextLevel == "" || $nextLevel == undefined) return;
            $nextLevel.html("");
            var options = "<option value=\"-1\">--请选择--</option>";
            for(var i = 0; i < data.length; i++){
                options += "<option value=\""+data[i].id+"\">"+data[i].categoryName+"</option>";

            }
            $nextLevel.html(options);
        },
        error:function(data){
        	alert(data)

            alert("加载分类失败！");
        }
    });
});
$(".checkApp").on("click",function(){
	var obj = $(this);
	var status = obj.attr("status");
	var vid = obj.attr("versionid");
	if(status == "1" && vid != "" && vid != null){//待审核状态下才可以进行审核操作
		window.location.href="check?aid="+ obj.attr("appinfoid") + "&vid="+ obj.attr("versionid");
	}else if(vid != "" || vid != null){
		alert("该APP应用没有上传最新版本,不能进行审核操作！");
	}else if(status != "1"){
		alert("该APP应用的状态为：【"+obj.attr("statusname")+"】,不能进行审核操作！");
	}
});



	
