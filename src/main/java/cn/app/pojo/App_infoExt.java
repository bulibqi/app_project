package cn.app.pojo;

public class App_infoExt extends App_info {

    private App_category app_category;
    private App_category app_category1;
    private App_category app_category2;
    private Data_dictionary data_dictionary;
    private Data_dictionary data_dictionary2;

    public Data_dictionary getData_dictionary2() {
        return data_dictionary2;
    }

    public void setData_dictionary2(Data_dictionary data_dictionary2) {
        this.data_dictionary2 = data_dictionary2;
    }

    private App_version app_version;
    private Dev_user dev_user;
    public App_category getApp_category1() {
        return app_category1;
    }

    public void setApp_category1(App_category app_category1) {
        this.app_category1 = app_category1;
    }

    public App_category getApp_category2() {
        return app_category2;
    }

    public void setApp_category2(App_category app_category2) {
        this.app_category2 = app_category2;
    }
    public Dev_user getDev_user() {
        return dev_user;
    }

    public void setDev_user(Dev_user dev_user) {
        this.dev_user = dev_user;
    }

    public App_category getApp_category() {
        return app_category;
    }

    public void setApp_category(App_category app_category) {
        this.app_category = app_category;
    }

    public Data_dictionary getData_dictionary() {
        return data_dictionary;
    }

    public void setData_dictionary(Data_dictionary data_dictionary) {
        this.data_dictionary = data_dictionary;
    }

    public App_version getApp_version() {
        return app_version;
    }

    public void setApp_version(App_version app_version) {
        this.app_version = app_version;
    }
}
