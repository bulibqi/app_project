package cn.app.pojo;

import java.math.BigDecimal;
import java.util.Date;

public class App_versionExt extends App_version{
    private App_info app_info;
    private Data_dictionary dataDictionary;

    public App_info getApp_info() {
        return app_info;
    }

    public void setApp_info(App_info app_info) {
        this.app_info = app_info;
    }

    public Data_dictionary getDataDictionary() {
        return dataDictionary;
    }

    public void setDataDictionary(Data_dictionary dataDictionary) {
        this.dataDictionary = dataDictionary;
    }
}