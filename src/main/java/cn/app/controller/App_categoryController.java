package cn.app.controller;

import cn.app.pojo.App_category;
import cn.app.service.App_categoryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/cate")
public class App_categoryController {
    @Resource
    private App_categoryService app_categoryService;
    @RequestMapping("/queryCate")
    @ResponseBody
    public Object queryCate(Long parentId){
        System.out.println(parentId);
        List<App_category> categoryList=app_categoryService.queryCategory(parentId);
        for (App_category d:categoryList
        ) {
            System.out.println(d.getCategoryName());
        }
        return categoryList;
    }

}
