package cn.app.controller;

import cn.app.pojo.*;
import cn.app.service.App_categoryService;
import cn.app.service.Backend_userService;
import cn.app.service.Data_dictionaryService;
import cn.app.service.impl.Backend_userServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

/**
 * @Author: Goku
 * @Date: 2018/12/14 19:48
 * @Description:
 */
@Controller
@RequestMapping(value = "/backend/")
public class Backend_userController {

    @Resource
    private Backend_userService userService;
    @Resource
    private App_categoryService app_categoryService;
    @Resource
    private Data_dictionaryService data_dictionaryService;

    @RequestMapping(value = "showLogin")
    public String showLogin() {
        return "backendlogin";
    }

    @RequestMapping(value = "login")
    public String login(String userCode, String userPassword, HttpServletRequest request) {
        Backend_user user = userService.userLogin(userCode, userPassword);
        if (user != null) {
            request.getSession().setAttribute("user", user);
            return "backend/main";
        } else {
            return "redirect:showLogin";
        }
    }

    @RequestMapping(value = "logOut")
    public String logOut() {
        return "redirect:/";
    }

    @RequestMapping("showAppInfo")
    public String queryApp(HttpServletRequest request, App_infoExt app_infoExt, String currentPageNo, String totalCount, String totalPageCount, String queryCategoryLevel1, String queryCategoryLevel2, String queryCategoryLevel3, String querySoftwareName, String queryStatus, String queryFlatformId) {

        if (querySoftwareName != null) {
            app_infoExt.setSoftwareName(querySoftwareName);
        }
        long staus = -1;
        if (queryStatus != null) {
            app_infoExt.setStatus(staus = Long.valueOf(queryStatus));
        }
        long FlatformId = -1;
        if (queryFlatformId != null) {
            app_infoExt.setFlatformId(FlatformId = Long.valueOf(queryFlatformId));

        }
        int pageSize = 5;
        int currentPageno = 1;
        if (currentPageNo != null) {
            currentPageno = Integer.valueOf(currentPageNo);
        }
        long parentId1 = -1;
        if (queryCategoryLevel1 != null) {
            parentId1 = Long.valueOf(queryCategoryLevel1);
            app_infoExt.setCategoryLevel1(parentId1);

        }
        long parentId2 = -1;
        if (queryCategoryLevel2 != null) {
            parentId2 = Long.valueOf(queryCategoryLevel2);
            app_infoExt.setCategoryLevel2(parentId2);
        }
        long parentId3 = -1;
        if (queryCategoryLevel3 != null) {
            parentId3 = Long.valueOf(queryCategoryLevel3);
            app_infoExt.setCategoryLevel3(parentId3);
        }
        List<Data_dictionary> statusList = data_dictionaryService.queryDic();
        List<Data_dictionary> flatFormList = data_dictionaryService.queryDicPt();
        List<App_category> categoryLevel1List = app_categoryService.queryCategory(null);
        List<App_category> categoryLevel2List = app_categoryService.queryCategory(parentId2);
        List<App_category> categoryLevel3List = app_categoryService.queryCategory(parentId3);
        HttpSession session = request.getSession();
        PageInfo<App_infoExt> pageInfo = userService.queryApp(app_infoExt, currentPageno, pageSize);
        session.setAttribute("appInfoList", pageInfo.getList());
        session.setAttribute("currentPageNo", pageInfo.getPageNum());
        session.setAttribute("totalCount", pageInfo.getTotal());
        session.setAttribute("totalPageCount", pageInfo.getPages());
        session.setAttribute("app", app_infoExt);
        session.setAttribute("statusList", statusList);
        session.setAttribute("flatFormList", flatFormList);
        session.setAttribute("categoryLevel1List", categoryLevel1List);
        session.setAttribute("categoryLevel2List", categoryLevel2List);
        session.setAttribute("categoryLevel3List", categoryLevel3List);
        session.setAttribute("queryCategoryLevel1", parentId1);
        session.setAttribute("queryCategoryLevel2", parentId2);
        session.setAttribute("queryCategoryLevel3", parentId3);
        return "/backend/applist";
    }


    @RequestMapping(value = "check")
    public String check(String aid, Model model) {
        App_infoExt appInfo = new App_infoExt();
        appInfo.setId(Long.valueOf(aid));
        App_infoExt app_infoExt = userService.selectOneByIdOfCheck(appInfo);
        model.addAttribute("appInfo", app_infoExt);
        return "backend/appcheck";
    }

    @RequestMapping(value = "checksave")
    public String checksave(String status, String id) {
        userService.changeStatus(status, id);
        return "redirect:showAppInfo";
    }

    @RequestMapping(value = "download")
    public ResponseEntity<byte[]> download(String filePath, HttpServletRequest request) throws IOException {
        String downPath = request.getServletContext().getRealPath("/statics/uploadfiles");
        File file = new File(downPath + File.pathSeparator + filePath);
        byte[] b = null;
        if (file.exists()) {
            b = FileUtils.readFileToByteArray(file);
        }
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        httpHeaders.setContentDispositionFormData("attachment", URLEncoder.encode(filePath, "UTF-8"));
        httpHeaders.setContentLength(file.length());
        ResponseEntity responseEntity = new ResponseEntity(b, httpHeaders, HttpStatus.OK);
        return responseEntity;

    }
}
