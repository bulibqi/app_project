package cn.app.controller;

import cn.app.pojo.App_infoExt;
import cn.app.pojo.App_version;
import cn.app.pojo.App_versionExt;
import cn.app.service.App_infoService;
import cn.app.service.impl.App_versionServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Author: Goku
 * @Date: 2018/12/17 19:44
 * @Description:
 */
@RequestMapping(value = "/version/")
@Controller
public class VersionController {

    @Resource
    private App_versionServiceImpl versionService;
    @Resource
    private App_infoService app_infoService;

    @RequestMapping(value="showAppVersionAdd")
    public String showAppVersionAdd(String id, Model model){
        List<App_versionExt> list = versionService.selectByPrimaryKey(Long.valueOf(id));
        if (list.size() == 0){
            App_versionExt app_versionExt = new App_versionExt();
            app_versionExt.setAppId(Long.valueOf(id));
            list.add(app_versionExt);
            model.addAttribute("appVersionList",list);
        }else {
            model.addAttribute("appVersionList",list);
        }

        return "developer/appversionadd";
    }

    @RequestMapping(value = "addversionsave")
    public String addversionsave(App_version app_version,String apkLocPath,String apkFileName,Long createdBy){

        app_version.setCreationDate(new Date());
        app_version.setApkLocPath(apkLocPath);
        app_version.setApkFileName(apkFileName);
        app_version.setCreatedBy(createdBy);
       boolean f= versionService.addAppVersion(app_version);
       if (f){
           Long versionId= versionService.queryByNew();
           app_infoService.updateAppVersionId(versionId,app_version.getAppId());
           return "redirect:/app/queryApp";
       }

        return "redirect:showAppVersionAdd";
    }

    @RequestMapping(value = "appversionmodify")
    public String appversionmodify(String aid, String vid, Model model){
        List<App_versionExt> list = versionService.selectByPrimaryKey(Long.valueOf(aid));
        App_versionExt appVersion = versionService.selectOneByMaxId(Long.valueOf(aid));
        model.addAttribute("appVersionList",list);
        model.addAttribute("appVersion",appVersion);
        return "developer/appversionmodify";

    }

    @RequestMapping(value = "appversionmodifysave")
    public String appversionmodifysave(App_version app_version,Long modifyBy){
        app_version.setModifyDate(new Date());
        app_version.setModifyBy(modifyBy);
        versionService.updateAppVersion(app_version);
        return "redirect:/app/queryApp";
    }

}
