package cn.app.controller;

import cn.app.pojo.Data_dictionary;
import cn.app.service.Data_dictionaryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/data")
public class Data_dictionaryController {
    @Resource
    private Data_dictionaryService data_dictionaryService;
    /**
     *
     *查看所属平台
     * */
    @RequestMapping("/queryDicPt")
    @ResponseBody
    public Object queryDicPt(){
        List<Data_dictionary> dataDictionaryList=data_dictionaryService.queryDicPt();
        return dataDictionaryList;
    }

}
