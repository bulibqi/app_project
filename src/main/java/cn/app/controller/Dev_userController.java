package cn.app.controller;

import cn.app.pojo.Dev_user;
import cn.app.service.Dev_userService;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * 开发者
 * */

@Controller
@RequestMapping("/dev")
public class Dev_userController {
    @Resource
    private Dev_userService dev_userService;

    /**
     * 跳转到开发者登录页面
     *
     * */
    @RequestMapping("/login")
    public String login(HttpServletRequest request){
        request.getSession().invalidate();
        return "/devlogin";
    }


    /**
     * 登录方法
     * */
    @RequestMapping("/devLogin")
    public String devLogin(HttpServletRequest request,String devCode, String devPassword){
        Dev_user dev_user= dev_userService.DevLogin(devCode,devPassword);
        if(dev_user!=null){
            request.getSession().setAttribute("user",dev_user);
            return "/developer/main";
        }
            return "redirect:/dev/login";
  }
  /**
   * 跳转到index页面
   * */
    @RequestMapping("/loginout")
    public String index(HttpServletRequest request){
        request.getSession().invalidate();
        return "redirect:/index.jsp";
    }
}
