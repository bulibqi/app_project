package cn.app.mapper;

import cn.app.pojo.Data_dictionary;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Data_dictionaryMapper {

    /**
     * 修改
     * */
    /**
     * 添加
     * */

    /**
     * 根据主键查询
     * */

    /**
     * 根据主键删除
     * */

    int deleteByPrimaryKey(Long id);

    int insert(Data_dictionary record);

    Data_dictionary selectByPrimaryKey(Long id);


    int updateByPrimaryKey(Data_dictionary record);

    /**
     * 查询app状态的信息
     *
     * */
    List<Data_dictionary> queryDic();
    /**
     * 查询所属平台的信息
     *
     * */
    List<Data_dictionary> queryDicPt();



}