package cn.app.mapper;

import cn.app.pojo.App_infoExt;
import cn.app.pojo.App_version;

import java.util.List;

import cn.app.pojo.App_versionExt;
import org.apache.ibatis.annotations.Param;

public interface App_versionMapper {



    /**
     * 根据主键删除
     * */
    int deleteByPrimaryKey(Long id);

    /**
     * 根据主键查询
     * */
    List<App_versionExt> queryByAppInfoId(@Param("id") Long id);

    App_versionExt selectByOne(@Param("id") Long id);

    int addAppVersion(App_version app_version);

    int updateAppVersion(App_version app_version);

    /**
     *
     * 查询版本的最新编号
     * */
    Long queryByNew();
}