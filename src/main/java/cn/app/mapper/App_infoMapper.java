package cn.app.mapper;

import cn.app.pojo.App_info;

import java.util.List;

import cn.app.pojo.App_infoExt;
import org.apache.ibatis.annotations.Param;

public interface App_infoMapper {

    /**
     *
     * 分页查询商品信息
     * */
    List<App_infoExt> queryApp(App_infoExt app_infoExt);
    /**
     * 根据主键删除
     * */
    int deleteByPrimaryKey(Long id);
    /**
     * 添加
     * */
    int insertApp(App_info app_info);

    /**
     * 根据主键查询
     * */
    App_infoExt selectByPrimaryKey(Long id);

    /**
     * 修改
     * */
    int updateByPrimaryKey(App_info appInfo);

    /**
     * 查看所有的APKName
     * */
    List<App_info> queryAPKName();

    /**
     * 修改APP审核状态
     * @param app_info
     * @return
     */
    int updateAppStatus(App_info app_info);
    /**
     * 根据编号修改app版本id
     * */
    int updateAppVersionId(@Param("versionId") Long versionId,@Param("id") Long id);
    /**
     * 根据编号删除app图片
     * */
    int updateAppPic(@Param("logoPicPath") String logoPicPath,@Param("id") Long id);
}