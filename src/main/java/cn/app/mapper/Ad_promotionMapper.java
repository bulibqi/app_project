package cn.app.mapper;

import cn.app.pojo.Ad_promotion;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Ad_promotionMapper {
   /**
    * 根据主键删除
    * */
    int deleteByPrimaryKey(Long id);

    /**
     * 添加
     * */
    int insert(Ad_promotion record);

    /**
     * 根据主键查询
     * */
    Ad_promotion selectByPrimaryKey(Long id);

    /**
     * 修改
     * */
    int updateByPrimaryKey(Ad_promotion record);
}