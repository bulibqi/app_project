package cn.app.mapper;

import cn.app.pojo.App_info;
import cn.app.pojo.App_infoExt;
import cn.app.pojo.Backend_user;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Backend_userMapper {
    /**
     * 根据条件查询用户信息
     * @param user
     * @return
     */
    Backend_user selectByQuery(Backend_user user);

    /**
     *
     * 分页查询商品信息
     * */
    List<App_infoExt> queryApp(App_infoExt app_infoExt);

    App_infoExt selectOneByIdOfCheck(App_info app_info);

    int changeAppStatus (App_info app_info);
}