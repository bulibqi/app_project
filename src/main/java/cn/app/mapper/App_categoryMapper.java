package cn.app.mapper;

import cn.app.pojo.App_category;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface App_categoryMapper {

    /**
     * 修改
     * */
    /**
     * 添加
     * */

    /**
     * 根据主键查询
     * */

    /**
     * 根据主键删除
     * */
    int deleteByPrimaryKey(Long id);

    int insert(App_category record);


    App_category selectByPrimaryKey(Long id);


    int updateByPrimaryKey(App_category record);

    /**
     * 查询分类信息
     * */
    List<App_category> queryCategory(@Param("parentId") Long parentId);

}