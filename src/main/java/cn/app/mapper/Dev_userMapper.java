package cn.app.mapper;

import cn.app.pojo.Dev_user;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Dev_userMapper {

    /***
     * 登录方法
     */
    Dev_user DevLogin(@Param("devCode") String devCode,@Param("devPassword")String devPassword);




    /**
     * 根据主键删除
     * */
    int deleteByPrimaryKey(Long id);

    /**
     * 添加
     * */
    int insert(Dev_user record);

    /**
     * 根据主键查询
     * */

    Dev_user selectByPrimaryKey(Long id);

    /**
     * 修改
     * */
    int updateByPrimaryKey(Dev_user record);
}