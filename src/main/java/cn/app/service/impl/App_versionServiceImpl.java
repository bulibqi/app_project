package cn.app.service.impl;

import cn.app.mapper.App_versionMapper;
import cn.app.pojo.App_infoExt;
import cn.app.pojo.App_version;
import cn.app.pojo.App_versionExt;
import cn.app.service.App_versionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class App_versionServiceImpl implements App_versionService {
    @Resource
    private App_versionMapper app_versionMapper;
    @Override
    public int deleteByPrimaryKey(Long id) {
        return app_versionMapper.deleteByPrimaryKey(id);
    }

    @Override
    public boolean addAppVersion(App_version app_version) {
        return (app_versionMapper.addAppVersion(app_version))>0;
    }

    @Override
    public List<App_versionExt> selectByPrimaryKey(Long id) {
        return app_versionMapper.queryByAppInfoId(id);
    }

    @Override
    public App_versionExt selectOneByMaxId(Long id) {
        return app_versionMapper.selectByOne(id);
    }

    @Override
    public boolean updateAppVersion(App_version app_version) {
        return (app_versionMapper.updateAppVersion(app_version))>0;
    }

    @Override
    public Long queryByNew() {
        return app_versionMapper.queryByNew();
    }


}
