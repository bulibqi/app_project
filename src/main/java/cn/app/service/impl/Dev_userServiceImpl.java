package cn.app.service.impl;

import cn.app.mapper.Dev_userMapper;
import cn.app.pojo.Dev_user;
import cn.app.service.Dev_userService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service()
public class Dev_userServiceImpl implements Dev_userService {
    @Resource
    private Dev_userMapper dev_userMapper;
    @Override
    public Dev_user DevLogin(String devCode, String devPassword) {
        return dev_userMapper.DevLogin(devCode,devPassword);
    }
}
