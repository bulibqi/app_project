package cn.app.service.impl;

import cn.app.mapper.Backend_userMapper;
import cn.app.pojo.App_info;
import cn.app.pojo.App_infoExt;
import cn.app.pojo.Backend_user;
import cn.app.service.Backend_userService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: Goku
 * @Date: 2018/12/14 19:39
 * @Description:
 */
@Service
public class Backend_userServiceImpl implements Backend_userService {
    @Resource
    private Backend_userMapper userMapper;


    @Override
    public Backend_user userLogin(String userCode, String userPassword) {
        Backend_user user = new Backend_user();
        if (userCode != null && userCode != "" && userPassword != null && userPassword != "") {
            user.setUserCode(userCode);
            user.setUserPassword(userPassword);
            user = userMapper.selectByQuery(user);
        }
        return user;
    }

    @Override
    public PageInfo<App_infoExt> queryApp(App_infoExt app_infoExt, int currentPageNo, int pageSize) {
        pageSize=5;
        PageHelper.startPage(currentPageNo,pageSize);
        List<App_infoExt> appInfoExtList=userMapper.queryApp(app_infoExt);
        PageInfo<App_infoExt> pageInfo=new PageInfo<>(appInfoExtList);
        return pageInfo;
    }

    @Override
    public App_infoExt selectOneByIdOfCheck(App_info app_info) {
        return userMapper.selectOneByIdOfCheck(app_info);
    }


    @Override
    public boolean changeStatus(String status,String id) {
        if (status == null || id == null){
            return false;
        }else {
            App_info appInfo = new App_info();
            appInfo.setId(Long.valueOf(id));
            appInfo.setStatus(Long.valueOf(status));
            return (userMapper.changeAppStatus(appInfo))>0;
        }
    }
}
