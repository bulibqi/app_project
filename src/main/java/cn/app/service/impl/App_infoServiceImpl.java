package cn.app.service.impl;

import cn.app.mapper.App_infoMapper;
import cn.app.pojo.App_info;
import cn.app.pojo.App_infoExt;
import cn.app.service.App_infoService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class App_infoServiceImpl implements App_infoService {
    @Resource
    private App_infoMapper app_infoMapper;

    @Override
    public PageInfo<App_infoExt> queryApp(App_infoExt app_infoExt, int currentPageNo, int pageSize) {
        pageSize = 5;
        PageHelper.startPage(currentPageNo, pageSize);
        List<App_infoExt> appInfoExtList = app_infoMapper.queryApp(app_infoExt);
        PageInfo<App_infoExt> pageInfo = new PageInfo<>(appInfoExtList);
        return pageInfo;
    }

    @Override
    public int insertApp(App_info app_info) {
        return app_infoMapper.insertApp(app_info);
    }

    @Override
    public List<App_info> queryAPKName() {
        return app_infoMapper.queryAPKName();
    }

    @Override
    public int deleteByPrimaryKey(Long id) {
        return app_infoMapper.deleteByPrimaryKey(id);
    }

    @Override
    public App_infoExt selectByPrimaryKey(Long id) {
        return app_infoMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(App_info appInfo) {
        return app_infoMapper.updateByPrimaryKey(appInfo);
    }

    @Override
    public boolean updateAppStatus(App_info app_info) {
        return (app_infoMapper.updateAppStatus(app_info)) > 0;
    }

    @Override
    public int updateAppVersionId(Long versionId, Long id) {
        return app_infoMapper.updateAppVersionId(versionId,id);
    }

    @Override
    public int updateAppPic(String logoPicPath, Long id) {
        return app_infoMapper.updateAppPic(logoPicPath,id);
    }


}
