package cn.app.service.impl;

import cn.app.mapper.App_categoryMapper;
import cn.app.pojo.App_category;
import cn.app.service.App_categoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class App_categoryServiceImpl implements App_categoryService {
    @Resource
    private App_categoryMapper app_categoryMapper;

    @Override
    public List<App_category> queryCategory(Long parentId) {
        return app_categoryMapper.queryCategory(parentId);
    }
}
