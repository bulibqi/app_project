package cn.app.service.impl;

import cn.app.mapper.Data_dictionaryMapper;
import cn.app.pojo.Data_dictionary;
import cn.app.service.Data_dictionaryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class Data_dictionaryServiceImpl implements Data_dictionaryService {
    @Resource
    private Data_dictionaryMapper data_dictionaryMapper;
    @Override
    public List<Data_dictionary> queryDic() {
        return data_dictionaryMapper.queryDic();
    }

    @Override
    public List<Data_dictionary> queryDicPt() {
        return data_dictionaryMapper.queryDicPt();
    }
}
