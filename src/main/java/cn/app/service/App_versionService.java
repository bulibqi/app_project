package cn.app.service;

import cn.app.pojo.App_infoExt;
import cn.app.pojo.App_version;
import cn.app.pojo.App_versionExt;

import java.util.List;

public interface App_versionService {


    /**
     * 根据主键删除
     * */
    int deleteByPrimaryKey(Long id);
    /**
     * 添加
     * */
    boolean addAppVersion(App_version app_version);

    /**
     * 根据主键查询
     * */
    List<App_versionExt> selectByPrimaryKey(Long id);

    /**
     * 根据app_version表中的appId查询
     * @param id
     * @return
     */
    App_versionExt selectOneByMaxId(Long id);

    /**
     * 修改
     * */
    boolean updateAppVersion(App_version app_version);
    /**
     *
     * 查询版本的最新编号
     * */
    Long queryByNew();
}
