package cn.app.service;

import cn.app.pojo.App_info;
import cn.app.pojo.App_infoExt;
import cn.app.pojo.Backend_user;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @Author: Goku
 * @Date: 2018/12/14 19:38
 * @Description:
 */
public interface Backend_userService {

    /**
     * 用户登录
     * @param userCode
     * @param userPassword
     * @return
     */
    Backend_user userLogin(String userCode ,String userPassword);

    /**
     *
     * 分页查询商品信息
     * */
    PageInfo<App_infoExt> queryApp(App_infoExt app_infoExt, int currentPageNo, int pageSize);

    App_infoExt selectOneByIdOfCheck(App_info app_info);

    boolean changeStatus (String status,String id);


}
