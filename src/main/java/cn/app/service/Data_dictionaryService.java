package cn.app.service;

import cn.app.pojo.Data_dictionary;

import java.util.List;

public interface Data_dictionaryService {

    /**
     * 查询app状态的信息
     *
     * */
    List<Data_dictionary> queryDic();
    /**
     * 查询所属平台的信息
     *
     * */
    List<Data_dictionary> queryDicPt();
}
