package cn.app.service;

import cn.app.pojo.App_info;
import cn.app.pojo.App_infoExt;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface App_infoService {
    /**
     *
     * 分页查询商品信息
     * */
    PageInfo<App_infoExt> queryApp(App_infoExt app_infoExt, int currentPageNo, int pageSize);
    /**
     * 添加
     * */
    int insertApp(App_info app_info);
    /**
     * 查看所有的APKName
     * */
    List<App_info> queryAPKName();
    /**
     * 根据主键删除
     * */
    int deleteByPrimaryKey(Long id);
    /**
     * 根据主键查询
     * */
    App_infoExt selectByPrimaryKey(Long id);
    /**
     * 修改app信息
     * */
    int updateByPrimaryKey(App_info appInfo);

    /**
     * 修改APP审核状态
     * @param app_info
     * @return
     */
    boolean updateAppStatus(App_info app_info);
    /**
     * 根据编号修改app版本id
     * */
    int updateAppVersionId(@Param("versionId") Long versionId, @Param("id") Long id);
    /**
     * 根据编号删除app图片
     * */
    int updateAppPic(@Param("logoPicPath") String logoPicPath,@Param("id") Long id);


}
