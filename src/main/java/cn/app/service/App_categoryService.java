package cn.app.service;

import cn.app.pojo.App_category;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface App_categoryService {
    /**
     * 查询分类信息
     * */
    List<App_category> queryCategory(@Param("parentId") Long parentId);

}
