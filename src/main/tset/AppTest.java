import cn.app.mapper.Dev_userMapper;
import cn.app.pojo.Backend_user;
import cn.app.pojo.Dev_user;
import cn.app.service.Dev_userService;
import cn.app.service.impl.Backend_userServiceImpl;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.annotation.Resource;

public class AppTest {
//
//    public static void main(String[] args) {
//        Backend_userServiceImpl userService = new Backend_userServiceImpl();
//        Backend_user user = userService.userLogin("admin","123456");
//        System.out.println(user.getUserCode());
//    }

    public static void main(String[] args) {
        ApplicationContext applicationContext=new ClassPathXmlApplicationContext("spring.xml");
        String name="test001";
        String pwd="123456";
        Dev_user dev_user= applicationContext.getBean(Dev_userService.class).DevLogin(name,pwd);
        System.out.println(dev_user);
    }
}
